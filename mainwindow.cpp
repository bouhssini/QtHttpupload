#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :  QMainWindow(parent),  ui(new Ui::MainWindow), startUp(false)
{
    ui->setupUi(this);

}

void MainWindow::Progress(qint64 bytesSent, qint64 bytesTotal) {
    qreal n = (qreal)bytesSent/(qreal)bytesTotal;
   ui->progressBar->setValue(n * 100);
    qDebug() << "Uploaded " << bytesSent << " of " << bytesTotal;
}

void MainWindow::uploadDone() {
    QString txt;
    int nu = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (nu == 200){
        txt = "Finished: " + QString::number( nu ) + "\n";
        txt += reply->readAll().trimmed();
        //ui->label->setText("");

    }else{
        txt = "Error: " + reply->errorString() + " nu:" + QString::number( nu ) + "\n";
    }
    qDebug() << txt;
    ui->la2->setText(txt);
    ui->pushButton->setText("ارفع");
    ui->pushButton_2->setEnabled(true);
    startUp = false;

    reply->deleteLater();
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    //ui->pushButton->setEnabled(false);
    if (!startUp){
       // ui->la2->setText("");
        ui->progressBar->setValue(0);
        startUp = true;
        ui->pushButton_2->setEnabled(false);
        ui->pushButton->setText("إيقاف");
        QString fileName = ui->label->text();
        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
        /*
        QHttpPart textPart;
        textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
        textPart.setBody("my text");
    //multiPart->append(textPart);
        */
        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/*"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                            QVariant("form-data; name=\"userImage\";  filename=\""+fileName.split("/").last()+"\""));
        QFile *file = new QFile(fileName);
        file->open(QIODevice::ReadOnly);
        imagePart.setBodyDevice(file);
        file->setParent(multiPart);

        multiPart->append(imagePart);

        QUrl url("http://66.70.190.62/web/upload/upload.php");
        QNetworkRequest request(url);

        QNetworkAccessManager * manager = new QNetworkAccessManager();
        reply = manager->post(request, multiPart);
        multiPart->setParent(reply); // delete the multiPart with the reply
        connect(reply, SIGNAL(uploadProgress(qint64,qint64)), this, SLOT(Progress(qint64,qint64)));
        connect(reply, SIGNAL(finished()), this, SLOT(uploadDone()));
    }else{
        startUp = false;
        ui->pushButton->setText("ارفع");
        ui->pushButton_2->setEnabled(true);
        reply->close();
        ui->progressBar->setValue(0);
    }

}

void MainWindow::on_pushButton_2_clicked()
{
   QString fileName = QFileDialog::getOpenFileName(this,
                                                   tr("Open Image"),
                                                   "./",
                                                   "Image Files (*.doc *.docx *.pptx *.ppt *.xlsx);; All Files (*.*)");
    qDebug() << fileName;
    qDebug() << fileName.split("/").last();
    ui->label->setText(fileName);
    ui->la2->setText("");
}
